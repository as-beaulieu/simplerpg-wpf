﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRPGEngine
{
    public class RandomNumberGenerator
    {
        public int GenerateWithinRange(int min, int max)
        {
            var random = new Random();
            return random.Next(min, max + 1);
        }
    }
}
