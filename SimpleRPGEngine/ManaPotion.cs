﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRPGEngine
{
    public class ManaPotion : Item
    {
        private int AmountRestored { get; set; }

        public ManaPotion(int id, string name, string namePlural, int amount)
            : base(id, name, namePlural)
        {
            this.AmountRestored = amount;
        }
    }
}
