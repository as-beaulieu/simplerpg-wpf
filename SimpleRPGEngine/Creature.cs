﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRPGEngine
{
    public class Creature
    {
        public int CurrentHitPoints { get; set; }
        public int MaximumHitPoints { get; set; }
        public int CurrentMana { get; set; }
        public int MaximumMana { get; set; }

        public Creature(int maxHp, int maxMana)
        {
            this.CurrentHitPoints = this.MaximumHitPoints = maxHp;
            this.CurrentMana = this.MaximumMana = maxMana;
        }

        public void RemoveHitPoints(int amount)
        {
            CurrentHitPoints -= amount;
            if (CurrentHitPoints < 0)
                CurrentHitPoints = 0;
        }

        public void RestoreHitPoints(int amount)
        {
            CurrentHitPoints += amount;
            if (CurrentHitPoints > MaximumHitPoints)
                CurrentHitPoints = MaximumHitPoints;
        }

        public void RemoveMana(int amount)
        {
            CurrentMana -= amount;
            if (CurrentMana < 0)
                CurrentMana = 0;
        }

        public void RestoreMana(int amount)
        {
            CurrentMana += amount;
            if (CurrentMana > MaximumMana)
                CurrentMana = MaximumMana;
        }
    }
}
