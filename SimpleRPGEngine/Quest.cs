﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRPGEngine
{
    public class Quest
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int XpReward { get; set; }
        public int GoldReward { get; set; }
        public QuestCompletionItem CompletionItem { get; set; }
        public Item RewardItem { get; set; }

        public Quest(int id, string name, string description, int xp, int gold)
        {
            this.ID = id;
            this.Name = name;
            this.Description = description;
            this.XpReward = xp;
            this.GoldReward = gold;
        }
    }
}
