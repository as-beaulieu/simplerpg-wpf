﻿using SimpleRPGEngine.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRPGEngine
{
    public static class World
    {
        public static readonly List<Item> Items = new List<Item>();
        public static readonly List<Monster> Monsters = new List<Monster>();
        public static readonly List<Quest> Quests = new List<Quest>();
        public static readonly List<Location> Locations = new List<Location>();

        static World()
        {
            PopulateItems();
            PopulateMonsters();
            PopulateQuests();
            PopulateLocations();
        }

        private static void PopulateLocations()
        {
            var home = new Location(
                LocationData.LOCATION_ID_HOME,
                "Home",
                "Home sweet home.");
            var townSquare = new Location(
                LocationData.LOCATION_ID_TOWN_SQUARE,
                "Town square",
                "There's a fountain in the center.");
            var alchemistHut = new Location(
                LocationData.LOCATION_ID_ALCHEMIST_HUT,
                "Alchemist's hut",
                "There are strange plants and potions on the shelves");
            var alchemistsGarden = new Location(
                LocationData.LOCATION_ID_ALCHEMISTS_GARDEN,
                "Alchemist's garden",
                "The garden is full of strange plants.");
            var farmhouse = new Location(
                LocationData.LOCATION_ID_FARMHOUSE,
                "Farmhouse",
                "A farmhouse, there is a large field and barn around the back.");
            var farmfield = new Location(
                LocationData.LOCATION_ID_FARM_FIELD,
                "Farmfield",
                "A large field where crops are growing. There is a farmhouse and barn nearby.");
            var guardPost = new Location(
                LocationData.LOCATION_ID_GUARD_POST,
                "Guard post",
                "The post appears to be manned by two city guards.");
            var bridge = new Location(
                LocationData.LOCATION_ID_BRIDGE,
                "Bridge",
                "There's a fast moving river under it.");
            var spiderField = new Location(
                LocationData.LOCATION_ID_SPIDER_FIELD,
                "The spider field",
                "There are webs everywhere!");

            //Linking locations together
            home.LocationNorth = townSquare;

            townSquare.LocationNorth = alchemistHut;
            townSquare.LocationSouth = home;
            townSquare.LocationEast = guardPost;
            townSquare.LocationWest = farmhouse;

            farmhouse.LocationEast = townSquare;
            farmhouse.LocationWest = farmfield;

            farmfield.LocationEast = farmhouse;
            farmfield.Monster = Monsters[0];

            alchemistHut.LocationNorth = alchemistsGarden;
            alchemistHut.LocationSouth = townSquare;

            alchemistsGarden.LocationSouth = alchemistHut;

            guardPost.LocationEast = bridge;
            guardPost.LocationWest = townSquare;

            bridge.LocationWest = guardPost;
            bridge.LocationEast = spiderField;

            spiderField.LocationWest = bridge;

            Locations.Add(home);
            Locations.Add(alchemistHut);
            Locations.Add(alchemistsGarden);
            Locations.Add(townSquare);
            Locations.Add(guardPost);
            Locations.Add(farmfield);
            Locations.Add(farmhouse);
            Locations.Add(bridge);
            Locations.Add(spiderField);
        }

        private static void PopulateQuests()
        {
            var clearAlchemistGarden = new Quest(
                QuestData.QUEST_ID_CLEAR_ALCHEMIST_GARDEN,
                "Clear the alchemist's garden",
                "Kill rats in the alchemist's garden and "
                + "bring back 3 rat tails. You will receive "
                + "a healing potion and 10 gold pieces",
                20, 10);
            clearAlchemistGarden.CompletionItem =
                new QuestCompletionItem(
                    ItemByID(ItemData.ITEM_ID_RAT_TAIL), 3);
            clearAlchemistGarden.RewardItem = ItemByID(ItemData.ITEM_ID_HEALING_POTION);

            var clearFarmersField = new Quest(
                QuestData.QUEST_ID_CLEAR_FARMERS_FIELD,
                "Clear the farmer's field",
                "Kill snakes in the farmer's field and "
                + "bring back 3 snake fangs. You will receive "
                + "an adventurer's pass and 20 gold pieces.", 20, 20);
            clearFarmersField.CompletionItem =
                new QuestCompletionItem(
                    ItemByID(ItemData.ITEM_ID_SNAKE_FANG), 3);
            clearFarmersField.RewardItem =
                ItemByID(ItemData.ITEM_ID_ADVENTURER_PASS);

            Quests.Add(clearAlchemistGarden);
            Quests.Add(clearFarmersField);
        }

        private static void PopulateMonsters()
        {
            var rat = new Monster(MonsterData.MONSTER_ID_RAT, "Rat", 5, 3, 10, 25, 3);
            rat.LootItems.Add(new LootItem(ItemByID(ItemData.ITEM_ID_RAT_TAIL), 75, false));
            rat.LootItems.Add(new LootItem(ItemByID(ItemData.ITEM_ID_PIECE_OF_FUR), 75, true));

            var snake = new Monster(MonsterData.MONSTER_ID_SNAKE, "Snake", 5, 3, 10, 50, 3);
            snake.LootItems.Add(new LootItem(ItemByID(ItemData.ITEM_ID_SNAKE_FANG), 75, false));
            snake.LootItems.Add(new LootItem(ItemByID(ItemData.ITEM_ID_SNAKE_SKIN), 75, true));

            var giantSpider = new Monster(MonsterData.MONSTER_ID_GIANT_SPIDER, "Giant Spider", 20, 5, 40, 75, 10);
            giantSpider.LootItems.Add(new LootItem(ItemByID(ItemData.ITEM_ID_SPIDER_FANG), 75, true));
            giantSpider.LootItems.Add(new LootItem(ItemByID(ItemData.ITEM_ID_SPIDER_SILK), 25, false));

            Monsters.Add(rat);
            Monsters.Add(snake);
            Monsters.Add(giantSpider);
        }

        private static void PopulateItems()
        {
            Items.Add(new Weapon(ItemData.ITEM_ID_RUSTY_SWORD, "Rusty sword", "Rusty swords", 0, 5));
            Items.Add(new Item(ItemData.ITEM_ID_RAT_TAIL, "Rat tail", "Rat tails"));
            Items.Add(new Item(ItemData.ITEM_ID_PIECE_OF_FUR, "Piece of fur", "Piece of fur"));
            Items.Add(new Item(ItemData.ITEM_ID_SNAKE_FANG, "Snake fang", "Snake fangs"));
            Items.Add(new Item(ItemData.ITEM_ID_SNAKE_SKIN, "Snakeskin", "Snakeskins"));
            Items.Add(new Weapon(ItemData.ITEM_ID_CLUB, "Club", "Clubs", 3, 10));
            Items.Add(new HealingPotion(ItemData.ITEM_ID_HEALING_POTION, "Healing potion", "Healing potions", 5));
            Items.Add(new Item(ItemData.ITEM_ID_SPIDER_FANG, "Spider fang", "Spider fangs"));
            Items.Add(new Item(ItemData.ITEM_ID_SPIDER_SILK, "Spider silk", "Spider silks"));
            Items.Add(new Item(ItemData.ITEM_ID_ADVENTURER_PASS, "Adventurer pass", "Adventurer passes"));
        }

        //public static T ByID(int id, T data)
        //{
        //    foreach(T item in )
        //}

        public static Item ItemByID(int id)
        {
            foreach (Item item in Items)
                if (item.ID == id)
                    return item;
            throw new Exception($"Item not found: {id}");
        }

        public static Monster MonsterById(int id)
        {
            foreach (Monster monster in Monsters)
                if (monster.ID == id)
                    return monster;
            throw new Exception($"monster not found: {id}");
        }

        public static Quest QuestById(int id)
        {
            foreach (Quest quest in Quests)
                if (quest.ID == id)
                    return quest;
            throw new Exception($"quest not found: {id}");
        }

        public static Location LocationById(int id)
        {
            foreach (Location location in Locations)
                if (location.ID == id)
                    return location;
            throw new Exception($"location not found: {id}");
        }
    }
}
