﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRPGEngine
{
    public class Monster : Creature
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int MaximumDamage { get; set; }
        public int XpReward { get; set; }
        public int GoldReward { get; set; }
        public List<LootItem> LootItems { get; set; }

        public Monster(int id, string name, int maxHp, int maxMana, int maxDamage, int xp, int gold)
            : base(maxHp, maxMana)
        {
            this.ID = id;
            this.Name = name;
            this.MaximumDamage = maxDamage;
            this.XpReward = xp;
            this.GoldReward = gold;
            this.LootItems = new List<LootItem>();
        }
    }
}
