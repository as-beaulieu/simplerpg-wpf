﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRPGEngine
{
    public class InventoryItem
    {
        public Item Item { get; set; }
        public int Quantity { get; set; }

        public InventoryItem(Item item, int amount)
        {
            this.Item = item;
            this.Quantity = amount;
        }
    }
}
