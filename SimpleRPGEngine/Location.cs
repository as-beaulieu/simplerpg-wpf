﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRPGEngine
{
    public class Location
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Item ItemToEnter { get; set; }
        public Monster Monster { get; set; }
        public Quest QuestAvailable { get; set; }
        public Location LocationNorth { get; set; }
        public Location LocationSouth { get; set; }
        public Location LocationEast { get; set; }
        public Location LocationWest { get; set; }

        public Location(
            int id, 
            string name, 
            string description)
        {
            this.ID = id;
            this.Name = name;
            this.Description = description;
        }
    }
}
