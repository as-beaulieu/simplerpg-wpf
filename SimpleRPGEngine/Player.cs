﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRPGEngine
{
    public class Player : Creature
    {
        
        public int Gold { get; set; }
        public int ExperiencePoints { get; set; }
        public int Level { get; set; }
        public List<InventoryItem> Inventory { get; set; }
        public List<PlayerQuest> ActiveQuests { get; set; }
        public Location CurrentLocation { get; set; }

        public Player (int maxHp, int maxMana,int gold, int xp) 
            : base (maxHp, maxMana)
        {
            this.Gold = gold;
            this.ExperiencePoints = xp;
            this.Level = 1;
            this.Inventory = new List<InventoryItem>();
            this.ActiveQuests = new List<PlayerQuest>();
        }

        public bool HasThisQuest(Quest quest)
        {
            foreach (var activeQuest in ActiveQuests)
                if (activeQuest.Details.ID == quest.ID)
                    return true;
            return false;
        }

        public bool CompletedThisQuest(Quest quest)
        {
            foreach (var activeQuest in ActiveQuests)
                if (activeQuest.Details.ID == quest.ID)
                    return activeQuest.IsCompleted;
            return false;
        }

        public bool HasAllQuestCompletionItems(Quest quest)
        {
            foreach(var inventoryItem in Inventory)
                if(inventoryItem.Item.ID == quest.CompletionItem.Details.ID 
                    && inventoryItem.Quantity >= quest.CompletionItem.Quantity)
                {
                    return true;
                }
            return false;
        }

        public bool HasRequiredItemToEnterLocation(Location location)
        {
            if (location.ItemToEnter == null)
                return true;
            
            foreach(var inventoryItem in Inventory)
                if (inventoryItem.Item.ID.Equals(location.ItemToEnter.ID))
                    return true;

            return false;
        }

        public void AddItemToInventory(Item item, int amount)
        {
            var itemInInventory = false;

            foreach(var inventoryItem in Inventory)
                if (item.ID.Equals(inventoryItem.Item.ID))
                {
                    inventoryItem.Quantity += amount;
                    itemInInventory = true;
                }

            if (!itemInInventory)
                Inventory.Add(new InventoryItem(item, amount));
        }

        public void AddExperience(int amount)
        {
            ExperiencePoints += amount;
            if(ExperiencePoints >= (Level * 100))
                LevelUp(10, 1);
        }

        private void LevelUp(int hpIncrease, int manaIncrease)
        {
            Level++;
            MaximumHitPoints += hpIncrease;
            MaximumMana += manaIncrease;
            CurrentHitPoints = MaximumHitPoints;
            CurrentMana = MaximumMana;
        }
    }
}
