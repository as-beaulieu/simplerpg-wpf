﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRPGEngine
{
    public class HealingPotion : Item
    {
        public int HealingAmount { get; set; }

        public HealingPotion(int id, string name, string namePlural, int amount) 
            : base(id, name, namePlural)
        {
            this.HealingAmount = amount;
        }
    }
}
