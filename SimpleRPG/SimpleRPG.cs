﻿using SimpleRPGEngine;
using SimpleRPGEngine.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleRPG
{
    public partial class SimpleRPG : Form
    {
        private Player _player;
        private Location _location;
        private Monster _currentMonster;

        public SimpleRPG()
        {
            InitializeComponent();
            _player = new Player(20, 5, 20, 0);
            _player.Inventory.Add(new InventoryItem(World.ItemByID(ItemData.ITEM_ID_RUSTY_SWORD), 1));
            _player.Inventory.Add(new InventoryItem(World.ItemByID(ItemData.ITEM_ID_HEALING_POTION), 1));
            MoveTo(World.LocationById(LocationData.LOCATION_ID_HOME));

            UpdatePlayerInformation();
        }

        private void SimpleRPG_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MoveTo(_player.CurrentLocation.LocationWest);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            MoveTo(_player.CurrentLocation.LocationEast);
        }

        private void dgvInventory_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvQuests_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void MoveTo(Location destination)
        {
            if(destination.ItemToEnter != null)
            {
                if (!_player.HasRequiredItemToEnterLocation(destination))
                {
                    rtbMessages.Text += "You must have a "
                        + destination.ItemToEnter
                        + " to enter this place!"
                        + Environment.NewLine;
                    ScrollToBottomOfMessagePanel();
                    return;
                }


                if(destination.QuestAvailable != null)
                {
                    if (_player.HasThisQuest(destination.QuestAvailable) 
                        && !_player.CompletedThisQuest(destination.QuestAvailable))
                    {
                        var playerHasAllItemsToCompleteQuest = true;

                        var foundItemInInventory = false;

                        foreach(var gear in _player.Inventory)
                            if (gear.Item.ID.Equals(destination.QuestAvailable.CompletionItem.Details.ID))
                            {
                                foundItemInInventory = true;
                                if(gear.Quantity < destination.QuestAvailable.CompletionItem.Quantity)
                                {
                                    playerHasAllItemsToCompleteQuest = false;
                                    break;
                                }
                                break;
                            }

                        if (!_player.HasRequiredItemToEnterLocation(destination))
                            playerHasAllItemsToCompleteQuest = false;

                        if (playerHasAllItemsToCompleteQuest)
                        {
                            rtbMessages.Text += Environment.NewLine;
                            rtbMessages.Text += "You have all the items to complete the "
                                + destination.QuestAvailable.Name
                                + " quest." + Environment.NewLine;
                            ScrollToBottomOfMessagePanel();

                            foreach (var item in _player.Inventory)
                                if (item.Item.ID.Equals(destination.QuestAvailable.CompletionItem.Details.ID))
                                {
                                    item.Quantity -= destination.QuestAvailable.CompletionItem.Quantity;
                                    break;
                                }
                        }

                        rtbMessages.Text += "You receive: " + Environment.NewLine;
                        rtbMessages.Text += destination.QuestAvailable.RewardItem.Name.ToString() + Environment.NewLine;
                        rtbMessages.Text += "as a reward, "
                            + destination.QuestAvailable.XpReward.ToString() +
                            " experience, " + Environment.NewLine;
                        rtbMessages.Text += " and "
                            + destination.QuestAvailable.GoldReward.ToString() + " gold." + Environment.NewLine;
                        ScrollToBottomOfMessagePanel();

                        _player.AddExperience(destination.QuestAvailable.XpReward);
                        //_player.ExperiencePoints += destination.QuestAvailable.XpReward;
                        _player.Gold += destination.QuestAvailable.GoldReward;

                        var addedItemToInventory = false;
                        foreach(var item in _player.Inventory)
                            if (item.Item.ID.Equals(destination.QuestAvailable.RewardItem.ID))
                            {
                                item.Quantity++;
                                addedItemToInventory = true;
                                break;
                            }

                        if (!addedItemToInventory)
                            _player.Inventory.Add(new InventoryItem(destination.QuestAvailable.RewardItem, 1));

                        foreach(var quest in _player.ActiveQuests)
                            if (quest.Details.ID.Equals(destination.QuestAvailable.ID))
                                quest.IsCompleted = true;
                    }
                }
                else
                {
                    rtbMessages.Text += "You receive the quest to " + Environment.NewLine;
                    rtbMessages.Text += destination.QuestAvailable.Description.ToString() + Environment.NewLine;
                    rtbMessages.Text += "To complete, gather " + destination.QuestAvailable.CompletionItem.Quantity.ToString()
                        + " of " + destination.QuestAvailable.CompletionItem.Details.Name.ToString() + Environment.NewLine;
                    ScrollToBottomOfMessagePanel();

                    _player.ActiveQuests.Add(new PlayerQuest(destination.QuestAvailable));
                }
            }

            _player.CurrentLocation = destination;

            btnMoveNorth.Visible = (destination.LocationNorth != null);
            btnMoveSouth.Visible = (destination.LocationSouth != null);
            btnMoveEast.Visible = (destination.LocationEast != null);
            btnMoveWest.Visible = (destination.LocationWest != null);

            rtbLocation.Text = destination.Name + Environment.NewLine;
            rtbLocation.Text += destination.Description + Environment.NewLine;

            if (destination.ID.Equals(LocationData.LOCATION_ID_HOME))
            {
                _player.CurrentHitPoints = _player.MaximumHitPoints;
                _player.CurrentMana = _player.MaximumMana;
            }

            UpdatePlayerInformation();

            if (destination.Monster != null)
            {
                rtbMessages.Text += "You see a " + destination.Monster.Name + "!" + Environment.NewLine;
                ScrollToBottomOfMessagePanel();

                var monster = World.MonsterById(destination.Monster.ID);
                _currentMonster = new Monster(
                    monster.ID,
                    monster.Name,
                    monster.MaximumHitPoints,
                    monster.MaximumMana,
                    monster.MaximumDamage,
                    monster.XpReward,
                    monster.GoldReward);
                foreach (var lootItem in monster.LootItems)
                    _currentMonster.LootItems.Add(lootItem);

                PotionVisible(true);
                WeaponVisible(true);

                //refresh player inventory
                UpdateInventoryInUi();

                //refresh player quest list
                UpdateQuestListInUi();

                UpdateUsableItemsInUi();
            }
        }

        private void btnMoveNorth_Click(object sender, EventArgs e)
        {
            MoveTo(_player.CurrentLocation.LocationNorth);
        }

        private void btnMoveSouth_Click(object sender, EventArgs e)
        {
            MoveTo(_player.CurrentLocation.LocationSouth);
        }

        private void btnUseWeapon_Click(object sender, EventArgs e)
        {
            var currentWeapon = (Weapon)cboWeapons.SelectedItem;

            var random = new RandomNumberGenerator();
            var damageDealt = random.GenerateWithinRange(currentWeapon.MinDamage, currentWeapon.MaxDamage);

            _currentMonster.CurrentHitPoints -= damageDealt;

            rtbMessages.Text += "You hit the " + _currentMonster.Name
                + " with your " + cboWeapons.SelectedItem.ToString()
                + " for " + damageDealt + " damage." + Environment.NewLine;
            ScrollToBottomOfMessagePanel();

            if (_currentMonster.CurrentHitPoints <= 0)
            {
                rtbMessages.Text += "You have defeated the " + _currentMonster.Name
                    + "!" + Environment.NewLine;

                _player.AddExperience(_currentMonster.XpReward);
                //_player.ExperiencePoints += _currentMonster.XpReward;
                rtbMessages.Text += "You receive " + _currentMonster.XpReward
                    + " experience." + Environment.NewLine;

                _player.Gold += _currentMonster.GoldReward;
                rtbMessages.Text += "You gather " + _currentMonster.GoldReward
                    + " gold from the monster." + Environment.NewLine;
                ScrollToBottomOfMessagePanel();

                var lootedItems = new List<InventoryItem>();
                foreach(var lootItem in _currentMonster.LootItems)
                    if(random.GenerateWithinRange(1, 100) <= lootItem.DropPercentage)
                        lootedItems.Add(new InventoryItem(lootItem.Details, 1));

                if(lootedItems.Count == 0)
                    foreach(var lootItem in _currentMonster.LootItems)
                        if (lootItem.IsDefaultItem)
                            lootedItems.Add(new InventoryItem(lootItem.Details, 1));

                foreach(var inventoryItem in lootedItems)
                {
                    _player.AddItemToInventory(inventoryItem.Item, inventoryItem.Quantity);

                    rtbMessages.Text += "You loot " + inventoryItem.Quantity.ToString() + " of "
                        + inventoryItem.Item.Name + "." + Environment.NewLine;
                    ScrollToBottomOfMessagePanel();
                }

                // Refresh player information and inventory controls   
                UpdatePlayerInformation();
                UpdateInventoryInUi();
                UpdateUsableItemsInUi();
                WeaponVisible(false);

                _currentMonster = null;

                rtbMessages.Text += Environment.NewLine;
                ScrollToBottomOfMessagePanel();
            }
            else
            {
                MonsterAttacks();
            }
        }

        private void UpdateInventoryInUi()
        {
            dgvInventory.RowHeadersVisible = false;

            dgvInventory.ColumnCount = 2;
            dgvInventory.Columns[0].Name = "Name";
            dgvInventory.Columns[0].Width = 197;
            dgvInventory.Columns[1].Name = "Quantity";

            dgvInventory.Rows.Clear();

            foreach(var inventoryItem in _player.Inventory)
                if(inventoryItem.Quantity > 0)
                    dgvInventory.Rows.Add(new[] { inventoryItem.Item.Name, inventoryItem.Quantity.ToString() });
        }

        private void UpdateQuestListInUi()
        {
            dgvQuests.RowHeadersVisible = false;
            dgvQuests.ColumnCount = 2;
            dgvQuests.Columns[0].Name = "Name";
            dgvQuests.Columns[0].Width = 197;
            dgvQuests.Columns[1].Name = "Completed";
            dgvQuests.Columns.Clear();
            foreach (var quest in _player.ActiveQuests)
                dgvQuests.Rows.Add(new[] { quest.Details.Name, quest.IsCompleted.ToString() });
        }

        private void UpdateUsableItemsInUi()
        {
            var weapons = new List<Weapon>();
            var potions = new List<HealingPotion>();
            foreach (var item in _player.Inventory)
            {
                if (item.Item is Weapon)
                    if (item.Quantity > 0)
                        weapons.Add((Weapon)item.Item);
                if (item.Item is HealingPotion)
                    if (item.Quantity > 0)
                        potions.Add((HealingPotion)item.Item);
            }

            if (weapons.Count == 0)
            {
                WeaponVisible(false);
            }
            else
            {
                cboWeapons.DataSource = weapons;
                cboWeapons.DisplayMember = "Name";
                cboWeapons.ValueMember = "ID";
                cboWeapons.SelectedIndex = 0;
            }

            if (potions.Count == 0)
            {
                PotionVisible(false);
            }
            else
            {
                cboPotions.DataSource = potions;
                cboPotions.DisplayMember = "Name";
                cboPotions.ValueMember = "ID";
                cboPotions.SelectedIndex = 0;
            }
        }

        private void btnUsePotion_Click(object sender, EventArgs e)
        {
            var potion = (HealingPotion)cboPotions.SelectedItem;

            _player.RestoreHitPoints(potion.HealingAmount);

            foreach (var inventoryItem in _player.Inventory)
                if (inventoryItem.Item.ID.Equals(potion.ID))
                {
                    inventoryItem.Quantity--;
                    break;
                }

            rtbMessages.Text += "You drank a healing potion." + Environment.NewLine;
            ScrollToBottomOfMessagePanel();

            if(_currentMonster != null)
                MonsterAttacks();

            UpdateUsableItemsInUi();
        }

        private void MonsterAttacks()
        {
            var random = new RandomNumberGenerator();
            var damageToPlayer = random.GenerateWithinRange(0, _currentMonster.MaximumDamage);
            _player.CurrentHitPoints -= damageToPlayer;

            rtbMessages.Text += "The " + _currentMonster.Name
                + " dealt " + damageToPlayer.ToString()
                + " damage to you." + Environment.NewLine;
            ScrollToBottomOfMessagePanel();

            lblHitPoints.Text = _player.CurrentHitPoints.ToString();

            if (_player.CurrentHitPoints <= 0)
            {
                rtbMessages.Text += "The creature has killed you." + Environment.NewLine;
                ScrollToBottomOfMessagePanel();
                _currentMonster = null;
                MoveTo(World.LocationById(LocationData.LOCATION_ID_HOME));
            }
        }

        private void UpdatePlayerInformation()
        {
            lblHitPoints.Text = _player.CurrentHitPoints.ToString();
            lblGold.Text = _player.Gold.ToString();
            lblExperience.Text = _player.ExperiencePoints.ToString();
            lblLevel.Text = _player.Level.ToString();
            lblMana.Text = _player.CurrentMana.ToString();
        }

        private void PotionVisible(bool isVisible)
        {
            cboPotions.Visible = isVisible;
            btnUsePotion.Visible = isVisible;
        }

        private void WeaponVisible(bool isVisible)
        {
            cboWeapons.Visible = isVisible;
            btnUseWeapon.Visible = isVisible;
        }

        private void ScrollToBottomOfMessagePanel()
        {
            rtbMessages.SelectionStart = rtbMessages.Text.Length;
            rtbMessages.ScrollToCaret();
        }
    }
}
